# OneNet – One network to rule them all: consensus network inference from microbiome data

This repository contains: 
* `Graphic_production.Rmd`
* `functions_graphic_production.R`
* `data_simulation.R`
* `functions_data_simulation.R`
* `algo_cluster/`

## Simulations and applications of the article

`Graphic_production.Rmd` was used to reproduce the figures used in the [OneNet article](https://www.biorxiv.org/content/10.1101/2023.05.05.539529v2), the result of which is available in the html vignette. 

## Functions used in the simulations

`functions_graphic_production.R` contains functions used to draw the figures of the article.

## Data simulation

`data_simulation.R` and `functions_data_simulation.R` were used to generate synthetic datasets and apply all inferences methods on them. The results are stored in the `data/` folder with the following important objects

* `data/HT_prev50.rds`: count table for the healthy individuals, after filtering MSPs using a 50% prevalence threshold
* `data/LC_prev50.rds`: count table for the patients affected by liver cirrhosis (LC), after filtering MSPs using a 50% prevalence threshold
* `data/vary_n_prev50.rds`: datasets of sizes n = 50, 100, 500, 1000 simulated from the LC patients counts using a prevalence filter of 50% and a known precision matrix derived from a cluster-like network
* `data/Inference_50_6337_ill.RData`: results of OneNet applied to the `LC_prev50` dataset. 
* `data/simu_*_prevXX_nYY.[rds|RData]`: datasets of sizes n = YY, filtered at prevalence threshold XX, simulated from the LC patients counts, using a known precision matrix derived from a cluster-like network
* `data/ZiLN_prevXX_YY.rds`: inferences using all methods up to ZiLN (all but COZINE) on the simulated dataset with n= YY and prevalence filter XX. 

## Clustering algorithm

* `algo_cluster/`: C++ code/headers used for core clustering